## Unreleased

### Added

### Changed

### Fixed

## [1.0.3] - 2024-09-20

Additon of OpenAvy and finalization of unsupervized installation

This revision adds installing the OpenAvy R package for standalone SNOWPACK calculations. It also controls the WRF setup's user input prompts better so that a fully unsupervized installation is now possible. If devops are created they now get some system monitoring software. A few other overall fixes and improvements.

## [1.0.2] - 2024-09-14

Installation of more custom packages and better webserver config

This minor revision includes installation of more of our Python and R packages since more powerful toolchains are running. The machine settings like the host are now passed cleanly through environment variables and getting credentials accross _AWSOME_ (if needed for some data source) is handled transparently, inluding a new GUI option for setting a password file. The webserver configuration has been improved.

## [1.0.1] - 2024-09-10

Version with all advertised installation process features working

This revision adds a few options (check out dev version, update _AWSOME_, devops, ...) and also some clarity (no more installer venv). The installation scripts have been improved and the pipeline is now running unobstructed until at least the heaviest of _AWSOME_'s modules.

## [1.0.0] - 2024-09-06

First public release of the installer

This first public version is a fully fledged one-click solution to installing _AWSOME_ on a server. It features a graphical user interface but can also be scripted. The GUI is principally general purpose and offers easy high level descriptions for input panel types which are used here to select the _AWSOME_ modules to install as well as some installation flags. The software installation itself is handled by a collection of well-designed modular bash scripts which the GUI controls and monitors.
