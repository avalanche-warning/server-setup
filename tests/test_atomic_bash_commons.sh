#!/usr/bin/env bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is a unit test for the AWSOME installer.

cwd=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source ${cwd}/../installer/scripts/common.sh
args=$(argparse $@)
read -r option_update option_develop option_reinstall <<< "$args"

# The testing suite passes cmd line args as used in the toolchain,
# check if these arrive correctly:
if [ ! "$option_update" = true ] ; then
	echo "--update mismatch"
	exit 1
fi
if [ "$option_develp" = true ] ; then
	echo "--develop mismatch"
	exit 1
fi
if [ ! "$option_reinstall" = true ] ; then
	echo "--reinstall mismatch"
	exit 1
fi
exit 0
