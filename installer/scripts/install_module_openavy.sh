#!/usr/bin/env bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the AWSOME installer. It installs OpenAvy.

set -e
set -x

cwd=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

. ${cwd}/install_r.sh
. ${cwd}/install_r_packages.sh
. ${cwd}/install_snowpack.sh

Rscript -e 'devtools::install_gitlab("avalanche-warning/data/fetchers/opensmeteo")'
Rscript -e 'devtools::install_gitlab("avalanche-warning/snow-cover/model-chains/openavy")'
