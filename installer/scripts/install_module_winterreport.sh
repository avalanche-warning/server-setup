#!/usr/bin/env bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the AWSOME installer.
# It installs our Python web services.

set -e
set -x

if [[ -z ${AWSOME_BASE} ]]; then
	export AWSOME_BASE=/opt/awsome/code
	echo "install_webapps.sh set AWSOME_BASE to $AWSOME_BASE"
fi
if [[ -z ${AWSOME_HOME} ]]; then
	export AWSOME_HOME=/home
	echo "install_webapps.sh set AWSOME_HOME to $AWSOME_HOME"
fi
if [[ -z ${AWSOME_VENVS} ]]; then
	export AWSOME_VENVS=/opt/awsome/venvs
	echo "install_webapps.sh set AWSOME_VENVS to $AWSOME_VENVS"
fi

apt-get install -y gfortran # for wrf-python

python3 -m venv $AWSOME_VENVS/winterreport
$AWSOME_VENVS/winterreport/bin/pip install -r ${AWSOME_BASE}/visualization/winter-report/requirements.txt
$AWSOME_VENVS/winterreport/bin/pip install ${AWSOME_BASE}/universal/packages/awsome-py
$AWSOME_VENVS/winterreport/bin/pip install ${AWSOME_BASE}/weather/wrf/awwrf/

sed "s@{AWSOME_HOME}@$AWSOME_HOME@g" ${AWSOME_BASE}/visualization/winter-report/winter_report.service.template > /etc/systemd/system/winter_report.service
sed -i "s@{AWSOME_VENVS}@$AWSOME_VENVS@g" /etc/systemd/system/winter_report.service
