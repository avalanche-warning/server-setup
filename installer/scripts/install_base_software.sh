#!/usr/bin/env bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the AWSOME installer.

set -e
set -x

apt-get update
apt-get install -y sudo adduser # Docker
apt-get install -y git
apt-get install -y python3 python3-pip python3-venv

apt-get install -y pkg-config wget
apt-get install -y sqlite3 libsqlite3-dev libcurl4-openssl-dev
apt-get install -y build-essential cmake libtbb-dev
apt-get install -y libnetcdf-dev libtiff-dev # SNOWPACK
