#!/usr/bin/env bash
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the AWSOME installer.
# It installs a web server with FastCGI and an interactive profile
# visualization tool.

set -e
set -x

if [[ -z ${AWSOME_BASE} ]]; then
	export AWSOME_BASE=/opt/awsome/code
	echo "install_module_snowcover.sh set AWSOME_BASE to $AWSOME_BASE"
fi

if [[ -z ${AWSOME_HOST} ]]; then
	export AWSOME_HOST=/opt/awsome/venvs
	echo "install_module_snowcover.sh set AWSOME_HOST to $AWSOME_HOST"
fi

if [[ -z ${AWSOME_VENVS} ]]; then
	export AWSOME_VENVS=/opt/awsome/venvs
	echo "install_module_snowcover.sh set AWSOME_VENVS to $AWSOME_VENVS"
fi

apt-get update
apt-get install -y curl wget unzip

cwds=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
server_root=/var/www/html
mkdir -p ${server_root}/landing
cp $cwds/../../sites/landing_page.html ${server_root}/landing
cp $cwds/../../sites/index.html ${server_root}
cp $cwds/../../sites/favicon.ico ${server_root}

apt-get install -y fcgiwrap libfcgi-dev
mkdir -p /opt/cgi-bin

wget --no-verbose -O niViz-2.0.zip https://code.wsl.ch/snow-models/niviz/-/package_files/453/download
rm -rf ./niViz-2.0
unzip niViz-2.0.zip
rm niViz-2.0.zip
sed -i "s|document.write('<base href=\"/\" />')|document.write('<base href=\"/niViz/\" />')|g" niViz-2.0/index.html
rm -rf ${server_root}/niViz
mv niViz-2.0 ${server_root}/niViz

apt-get install -y caddy
mkdir -p /etc/systemd/system/caddy.service.d/
cat <<EOF > /etc/systemd/system/caddy.service.d/override.conf
[Service]
Group=www-data
Environment="AWSOME_HOST=$AWSOME_HOST"
EOF
cp $cwds/../../sites/Caddyfile /etc/caddy/Caddyfile
