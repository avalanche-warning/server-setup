#!/usr/bin/env bash
################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script installs R packages globally.

set -e
set -x

if [[ -z ${AWSOME_BASE} ]]; then
	export AWSOME_BASE=/opt/awsome/code
	echo "install_r_packages.sh set AWSOME_BASE to $AWSOME_BASE"
fi

#install.packages(c("terra", "raster"))
apt-get install -y r-cran-devtools r-cran-ini r-cran-stringr
Rscript -e 'install.packages(c("configr"))'

# Awsome-R:
Rscript -e "devtools::install('${AWSOME_BASE}/universal/packages/awsome-r/awsome.ini')"
Rscript -e "devtools::install('${AWSOME_BASE}/universal/packages/awsome-r/awsome.smet')"
Rscript -e "devtools::install('${AWSOME_BASE}/universal/packages/awsome-r/awsome.pro')"

# Dependencies for snowpacktools.aggregatepro:
Rscript -e "devtools::install_bitbucket('sfu-arp/sarp.snowprofile', ref = 'master')"
# ++ Rscript -e 'devtools::install_bitbucket('\''sfu-arp/sarp.snowprofile'\'', ref = '\''master'\'')'
# Downloading bitbucket repo sfu-arp/sarp.snowprofile@master
# rlang      (1.1.3   -> 1.1.4 ) [CRAN]
# cli        (3.6.2   -> 3.6.3 ) [CRAN]
# data.table (1.14.10 -> 1.16.2) [CRAN]
# Installing 3 packages: rlang, cli, data.table
# Installing packages into '/usr/local/lib/R/site-library'
Rscript -e "devtools::install_bitbucket('sfu-arp/sarp.snowprofile.alignment', ref = 'master')"
Rscript -e "devtools::install_bitbucket('sfu-arp/sarp.snowprofile.pyface', ref = 'master')"
# ++ Rscript -e 'devtools::install_bitbucket('\''sfu-arp/sarp.snowprofile.pyface'\'', ref = '\''master'\'')'
# Downloading bitbucket repo sfu-arp/sarp.snowprofile.pyface@master
# Rcpp       (1.0.12 -> 1.0.13-1) [CRAN]
# withr      (2.5.0  -> 3.0.2   ) [CRAN]
# jsonlite   (1.8.8  -> 1.8.9   ) [CRAN]
# reticulate (1.35.0 -> 1.40.0  ) [CRAN]
# Installing 4 packages: Rcpp, withr, jsonlite, reticulate
# Installing packages into '/usr/local/lib/R/site-library'
