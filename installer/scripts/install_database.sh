#!/usr/bin/env bash
################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the AWSOME installer.
# It installs a database server.

set -e
set -x

cwd=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source ${cwd}/common.sh
args=$(argparse $@)
read -r option_update option_develop option_reinstall <<< "$args"

if [ "$option_reinstall" = true ] ; then
	apt-get -y purge mongodb-org*
	apt-get -y autoremove
	rm -r /var/lib/mongodb
	rm -r /var/log/mongodb
else
	if which mongod > /dev/null 2>&1; then
		echo "MongoDB is already installed."
		exit 0
	fi
fi

apt-get update
apt-get install -y curl gpg
distro=ubuntu
codename=jammy
component=multiverse
curl -fsSL https://www.mongodb.org/static/pgp/server-7.0.asc | \
    gpg -o /usr/share/keyrings/mongodb-server-7.0.gpg \
    --dearmor --yes
echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] https://repo.mongodb.org/apt/${distro} ${codename}/mongodb-org/7.0 ${component}" | \
    tee /etc/apt/sources.list.d/mongodb-org-7.0.list

apt-get update
apt-get install -y mongodb-org
# Sometimes the installer does not set correct rights resulting in systemctl status
# ExecStart=/usr/bin/mongod --config /etc/mongod.conf (code=exited, status=14):
chown -R mongodb:mongodb /var/lib/mongodb
if [ -f /tmp/mongodb-27017.sock ]; then
	chown mongodb:mongodb /tmp/mongodb-27017.sock
fi
