#!/usr/bin/env bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script updates the server software itself.

set -e
set -x

apt-get update && apt-get -y upgrade
