#!/usr/bin/env bash
################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script installs a specific version of R.

set -e
set -x

apt-get update
apt-get install -y curl
apt-get install -y libfreetype-dev libharfbuzz-dev libfribidi-dev
apt-get install -y software-properties-common
apt-get install -y libfontconfig1-dev

# we must install these two dependencies with specific versions manually:
libicu=libicu70_70.1-2_amd64.deb
curl -O http://ftp.osuosl.org/pub/ubuntu/pool/main/i/icu/$libicu
dpkg -i $libicu
rm $libicu
libtiff5=libtiff5_4.3.0-6_amd64.deb
curl -O http://archive.ubuntu.com/ubuntu/pool/main/t/tiff/$libtiff5
dpkg -i $libtiff5
rm $libtiff5

# update R: https://cran.r-project.org/bin/linux/ubuntu/
curl https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc -o /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
apt-key adv --keyserver keyserver.ubuntu.com --recv-key '95C0FAF38DB3CCAD0C080A7BDC78B2DDEABC47B7'
#apt-get install -y lsb-release
add-apt-repository -y "deb https://cloud.r-project.org/bin/linux/ubuntu jammy-cran40/"
apt-get install -y r-base-core
