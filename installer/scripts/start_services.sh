# after all installations, start daemons
# allows for containerization without system services

set -e
set -x

cp ${cwd}./../../sites/Caddyfile /etc/caddy/Caddyfile
systemctl reload caddy

systemctl daemon-reload
systemctl start mongod
systemctl enable winter_report
systemctl start winter_report
systemctl enable smp
systemctl start smp
