#!/usr/bin/env bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the AWSOME installer.

set -e
set -x

if [[ -z ${AWSOME_BASE} ]]; then
	export AWSOME_BASE=/opt/awsome/code
	echo "install_module_wrf.sh set AWSOME_BASE to $AWSOME_BASE"
fi
if [[ -z ${AWSOME_VENVS} ]]; then
	export AWSOME_VENVS=/opt/awsome/venvs
	echo "install_module_wrf.sh set AWSOME_VENVS to $AWSOME_VENVS"
fi

apt-get install csh
apt-get install -y gfortran
apt-get install -y expect # to answer prompts in the build process

awwrf_path=${AWSOME_BASE}/weather/wrf/awwrf
# make space for group weather users to build awwrf at:
mkdir -p $awwrf_path/build
chown root:weather $awwrf_path/build
chmod 775 $awwrf_path/build
mkdir -p $awwrf_path/awwrf.egg-info
chown root:weather $awwrf_path/awwrf.egg-info
chmod 775 $awwrf_path/awwrf.egg-info

python3 -m venv $AWSOME_VENVS/wrf
$AWSOME_VENVS/wrf/bin/pip install numpy
$AWSOME_VENVS/wrf/bin/pip install wheel
git clone https://github.com/NCAR/wrf-python.git /tmp/wrf-python # TODO: remove this once https://github.com/NCAR/wrf-python/pull/241 comes with pip
$AWSOME_VENVS/wrf/bin/pip install /tmp/wrf-python
rm -rf /tmp/wrf-python
$AWSOME_VENVS/wrf/bin/pip install ${AWSOME_BASE}/weather/wrf/awwrf/

# install and compile WRF and WPS:
su - wrf --whitelist-environment="AWSOME_BASE,AWSOME_VENVS,AWSOME_SMALL" -c ". ${AWSOME_BASE}/weather/wrf/setup/download_wrf.sh"
su - wrf --whitelist-environment="AWSOME_BASE,AWSOME_VENVS,AWSOME_SMALL" -c ". ${AWSOME_BASE}/weather/wrf/setup/install_libraries.sh"
su - wrf --whitelist-environment="AWSOME_BASE,AWSOME_VENVS,AWSOME_SMALL" -c ". ${AWSOME_BASE}/weather/wrf/setup/build_wrf_wps_unsupervized.sh"

