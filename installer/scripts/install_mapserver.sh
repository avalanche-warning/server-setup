#!/usr/bin/env bash
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# It installs MapServer with all available compilation options.

set -e
set -x

cwd=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source ${cwd}/common.sh
args=$(argparse $@)
read -r option_update option_develop option_reinstall <<< "$args"

if [ "$option_reinstall" = true ] ; then
	rm -f "/opt/mapserver/mapserver.conf"
	rm -rf ~/Mapserver
fi

if [ -f "/opt/mapserver/mapserver.conf" ]; then
	echo "MapServer is already installed."
	exit
fi

# install all we could ever need for MapServer:
apt-get install -y default-jdk mono-complete
apt-get install -y libcairo2-dev libprotobuf-c-dev protobuf-c-compiler librsvg2-dev swig
apt-get install -y libharfbuzz-dev libfribidi-dev

mapserv_install=/opt/mapserver
mapserv_conf=${mapserv_install}/mapserver.conf # parent folder must exist

cd
git clone https://github.com/MapServer/MapServer.git
cd MapServer
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=${mapserv_install} -DMAPSERVER_CONFIG_FILE=${mapserv_conf} -DWITH_CLIENT_WMS=ON ..
make
make install
cd ../..
rm -rf MapServer

mkdir -p /opt/cgi-bin
ln -s ${mapserv_install}/bin/mapserv /opt/cgi-bin/maps # more concise URL
rm -rf /etc/opt/mapserver # remove the sample configuration file

# MapServer needs access to our data so that we don't have to copy it all out:
#adduser www-data data
#adduser www-data snowpack

mapdir=${mapserv_install}/share/maps
mkdir -p ${mapdir}
datadir=${mapserv_install}/share/data
mkdir -p ${datadir}
