#!/usr/bin/env bash
################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the AWSOME installer.

set -e
set -x

if [[ -z ${AWSOME_BASE} ]]; then
	export AWSOME_BASE=/opt/awsome/code
	echo "install_module_smp.sh set AWSOME_BASE to $AWSOME_BASE"
fi
if [[ -z ${AWSOME_VENVS} ]]; then
	export AWSOME_VENVS=/opt/awsome/venvs
	echo "install_module_smp.sh set AWSOME_VENVS to $AWSOME_VENVS"
fi

cwd=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
. ${cwd}/common.sh
args=$(argparse $@)
read -r option_update option_develop option_reinstall <<< "$args"

cd ~
if [ "$option_reinstall" = true ] ; then
	rm -rf "/home/smp/snowmicropyn"
	rm -rf "/home/smp/src"
	rm -rf "$AWSOME_VENVS/smp"
fi

apt-get install -y gfortran # for wrf-python
su - smp --whitelist-environment="AWSOME_BASE,AWSOME_VENVS,AWSOME_SMALL" -c ". ${cwd}/install_snowpack.sh ${@:1}"

python3 -m venv $AWSOME_VENVS/smp
$AWSOME_VENVS/smp/bin/pip install -r ${AWSOME_BASE}/snow-cover/model-chains/smp-chain/requirements.txt
$AWSOME_VENVS/smp/bin/pip install ${AWSOME_BASE}/universal/packages/awsome-py
$AWSOME_VENVS/smp/bin/pip install ${AWSOME_BASE}/data/provider
$AWSOME_VENVS/smp/bin/pip install ${AWSOME_BASE}/snow-cover/postprocessing/snowpacktools
$AWSOME_VENVS/smp/bin/pip install ${AWSOME_BASE}/snow-cover/model-chains/smp-chain/awsmp
$AWSOME_VENVS/smp/bin/pip install ${AWSOME_BASE}/snow-cover/profsnow
$AWSOME_VENVS/smp/bin/pip install ${AWSOME_BASE}/weather/wrf/awwrf

if [ -d "/home/smp/snowmicropyn" ]; then
  echo "snowmicropyn already installed."
else
  su - smp -c "git clone https://github.com/slf-dot-ch/snowmicropyn.git"
  su - smp -c "cd snowmicropyn && git checkout develop"
fi
$AWSOME_VENVS/smp/bin/pip install ~smp/snowmicropyn

cp ${AWSOME_BASE}/snow-cover/model-chains/smp-chain/webservice/smp.service /etc/systemd/system

smp_output_path=/var/www/html/public/smp-chain
mkdir -p $smp_output_path
chown root:smp $smp_output_path
chmod 775 $smp_output_path
