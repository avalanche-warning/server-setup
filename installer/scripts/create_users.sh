#!/usr/bin/env bash
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the AWSOME installer.

set -e
set -x

if [[ -z ${AWSOME_BASE} ]]; then
	export AWSOME_BASE=/opt/awsome/code
	echo "create_users.sh set AWSOME_BASE to $AWSOME_BASE"
fi
if [[ -z ${AWSOME_VENVS} ]]; then
	export AWSOME_VENVS=/opt/awsome/venvs
	echo "create_users.sh set AWSOME_VENVS to $AWSOME_VENVS"
fi

cwdu=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source ${cwdu}/common.sh

accounts=opera,smp,snowpack,wrf # comma-separated list of operational users

IFS="," read -ra users <<< $accounts
for user in "${users[@]}"; do
	if ! user_exists "$user"; then
		adduser --home ${AWSOME_HOME}/${user} --disabled-password --gecos "" $user
	fi
done

# create groups for high level modules if they do not exist:
getent group data &>/dev/null || groupadd data
getent group snow-cover &>/dev/null || groupadd snow-cover
getent group universal &>/dev/null || groupadd universal
getent group visualization &>/dev/null || groupadd visualization
getent group weather &>/dev/null || groupadd weather

# these groups may share package build locations for example:
adduser snowpack snow-cover
adduser wrf weather
adduser smp snowpack # access to main snowpack chains' data

# at this point we can copy out the secrets file if specified (default is ~opera):
if [[ ! -z ${AWSOME_SECRETS} ]]; then
	secrets_dir="$(dirname "${AWSOME_SECRETS}")"
	mkdir -p $secrets_dir
	cp ${cwdu}/../../resources/awsome.ini $AWSOME_SECRETS
fi
