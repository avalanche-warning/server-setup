#!/usr/bin/env bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the AWSOME installer.

set -e
set -x

if [[ -z ${AWSOME_BASE} ]]; then
	export AWSOME_BASE=/opt/awsome/code
	echo "install_module_weather.sh set AWSOME_BASE to $AWSOME_BASE"
fi

cwd=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

su - snowpack --whitelist-environment="AWSOME_BASE,AWSOME_VENVS,AWSOME_SMALL" -c ". ${cwd}/install_snowpack.sh ${@:1}"

mkdir -p ${AWSOME_BASE}/weather/solar/build
chown root:snow-cover ${AWSOME_BASE}/weather/solar/build
chmod 775 ${AWSOME_BASE}/weather/solar/build
mkdir -p ${AWSOME_BASE}/weather/maps/build
chown root:snow-cover ${AWSOME_BASE}/weather/maps/build
chmod 775 ${AWSOME_BASE}/weather/maps/build

su - snowpack -c "cd ${AWSOME_BASE}/weather/solar && make"
su - snowpack -c "cd ${AWSOME_BASE}/weather/maps && make"
