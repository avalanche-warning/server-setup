#!/usr/bin/env bash
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the AWSOME installer.
# It downloads and installs MeteoIO and SNOWPACK.

set -e
set -x

cwd=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
. ${cwd}/common.sh
args=$(argparse $@)
read -r option_update option_develop option_reinstall <<< "$args"

cd ~
if [ "$option_reinstall" = true ] ; then
	rm -rf "src/meteoio"
	rm -rf "src/snowpack"
fi
mkdir -p src && cd src

if [ ! -d "meteoio" ]; then
	git clone https://gitlabext.wsl.ch/snow-models/meteoio.git
	cd meteoio
	cmake -S . -B ./build -DCMAKE_INSTALL_PREFIX=~/.local -DPROJ=ON -DPLUGIN_NETCDFIO=ON
	cmake --build build --target install
	cd ..
else
	echo "MeteoIO already installed."
fi

if [ ! -d "snowpack" ]; then
	git clone https://gitlabext.wsl.ch/snow-models/snowpack.git
	cd snowpack
	cmake -S . -B ./build -DCMAKE_INSTALL_PREFIX=~/.local
	cmake --build build --target install
else
	echo "SNOWPACK already installed."
fi
