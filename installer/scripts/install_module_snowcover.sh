#!/usr/bin/env bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the AWSOME installer.

set -e
set -x

cwd=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [[ -z ${AWSOME_BASE} ]]; then
	export AWSOME_BASE=/opt/awsome/code
	echo "install_module_snowcover.sh set AWSOME_BASE to $AWSOME_BASE"
fi
if [[ -z ${AWSOME_VENVS} ]]; then
	export AWSOME_VENVS=/opt/awsome/venvs
	echo "install_module_snowcover.sh set AWSOME_VENVS to $AWSOME_VENVS"
fi

spt_path="${AWSOME_BASE}/snow-cover/postprocessing/snowpacktools"

mkdir -p $spt_path/build # allow snow-cover users to build snowpacktools on site
chown root:snow-cover $spt_path/build
chmod 775 $spt_path/build
mkdir -p $spt_path/snowpacktools.egg-info
chown root:snow-cover $spt_path/snowpacktools.egg-info
chmod 775 $spt_path/snowpacktools.egg-info

. ${cwd}/install_r.sh
. ${cwd}/install_r_packages.sh ${@:1} # writes to /usr/local/lib
su - snowpack --whitelist-environment="AWSOME_BASE,AWSOME_VENVS,AWSOME_SMALL" -c ". ${cwd}/install_snowpack.sh ${@:1}"
su - snowpack --whitelist-environment="AWSOME_BASE,AWSOME_VENVS,AWSOME_SMALL" -c "echo RETICULATE_PYTHON=${AWSOME_VENVS}/snowpack/bin/python > .Renviron"

python3 -m venv $AWSOME_VENVS/snowpack
$AWSOME_VENVS/snowpack/bin/pip install setuptools
$AWSOME_VENVS/snowpack/bin/pip install ${AWSOME_BASE}/universal/packages/awsome-py
$AWSOME_VENVS/snowpack/bin/pip install ${AWSOME_BASE}/data/provider
$AWSOME_VENVS/snowpack/bin/pip install ${AWSOME_BASE}/snow-cover/postprocessing/snowpacktools
$AWSOME_VENVS/snowpack/bin/pip install ${AWSOME_BASE}/snow-cover/postprocessing/qmah
$AWSOME_VENVS/snowpack/bin/pip install -r ${AWSOME_BASE}/snow-cover/model-chains/gridded-chain/requirements.txt
$AWSOME_VENVS/snowpack/bin/pip install ${AWSOME_BASE}/snow-cover/profsnow
$AWSOME_VENVS/snowpack/bin/pip install paramiko scp  # for visualization/upload/upload.py
