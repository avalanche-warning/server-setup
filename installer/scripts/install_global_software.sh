#!/usr/bin/env bash
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the AWSOME installer.
# It installs some global development and generic software.

set -e
set -x

apt-get install -y proj-bin libproj-dev python3-pyproj
apt-get install -y gdal-bin libgdal-dev python3-gdal
apt-get install -y python3-dev
apt-get install -y libfontconfig1-dev
apt-get install -y curl
