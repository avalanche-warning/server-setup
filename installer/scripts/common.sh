#!/usr/bin/env bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This file provides common functionality across bash scripts.

argparse() { # parser for the installation flags that are available
	options=$(getopt -o udr --long update,develop,reinstall -- "$@")
	option_update=false
	option_develop=false
	option_reinstall=false

	eval set -- "$options"
	while [ : ]; do
		case "$1" in
			-u | --update)
				option_update=true
				shift
				;;
			-d | --develop)
				option_develop=true
				shift
				;;
			-r | --reinstall)
				option_reinstall=true
				shift
				;;
			--) shift;
				break
				;;
		esac
	done
	echo "$option_update $option_develop $option_reinstall"
	return 0
}

get_distro() {
	distro=$(lsb_release -is)
	distro=${distro,} # first char to lower case
	codename=$(lsb_release -cs)
	echo "$distro $codename"
	return 0
}

user_exists(){ id "$1" &>/dev/null; }
