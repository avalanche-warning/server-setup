#!/usr/bin/env bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script downloads AWSOME's full code base.

set -e
set -x

if [[ -z ${AWSOME_BASE} ]]; then
	export AWSOME_BASE=/opt/awsome/code
	echo "install_module_smp.sh set AWSOME_BASE to $AWSOME_BASE"
fi

if [ ! -d $AWSOME_BASE ]; then
	mkdir -p $AWSOME_BASE
fi

cwdupd=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source ${cwdupd}/common.sh
args=$(argparse $@)
read -r option_update option_develop option_reinstall <<< "$args"

reporoot="https://gitlab.com/avalanche-warning"
projects=(
	"data/fetchers/download"
	"data/fetchers/gfs"
	"data/fetchers/profile-obs"
	"data/fetchers/wiski"
	"data/static-data/dems/austria"
	"data/static-data/dems/nwp-model-grids"
	"data/static-data/shapefiles"
	"data/provider"
	"snow-cover/model-chains/gridded-chain"
	"snow-cover/model-chains/smp-chain"
	"snow-cover/model-chains/snowobs-chain"
	"snow-cover/model-chains/stations-chain"
	"snow-cover/postprocessing/validation/nwp-model-analysis"
	"snow-cover/postprocessing/validation/profile-similarity-assessment"
	"snow-cover/postprocessing/qmah"
	"snow-cover/postprocessing/snowpacktools"
	"snow-cover/preprocessing/metadata"
	"snow-cover/preprocessing/nwp-models"
	"snow-cover/profsnow"
	"universal/packages/awsome-py"
	"universal/packages/awsome-r/awsome.ini"
	"universal/packages/awsome-r/awsome.pro"
	"universal/packages/awsome-r/awsome.smet"
	"universal/server/server-setup"
	"universal/server/toolchain"
	"universal/user-settings/domains"
	"visualization/dashboard"
	"visualization/upload"
	"visualization/web"
	"visualization/winter-report"
	"visualization/wms"
	"weather/satellites/sentinel"
	"weather/maps"
	"weather/solar"
	"weather/wrf"
)

if [ "$option_reinstall" = true ]; then
	if [[ -z $AWSOME_BASE ]]; then
		echo "[E] AWSOME_BASE empty when it shouldn't be!"
		exit 1
	fi
	rm -rf $AWSOME_BASE/*
fi

for proj in "${projects[@]}"
do
	url=${reporoot}/${proj}.git
	path=${AWSOME_BASE}/$proj
	parent_path=${AWSOME_BASE}/$(dirname $proj)

	echo "Using git paths $path -> $parent_path"
	if [ -d $path ]; then
		if [ "$option_update" = true ]; then
			cd $path
			git pull
		fi
	else
		mkdir -p $parent_path
		cd $parent_path
		git clone ${url}
	fi
	dev_branch_name=dev
	if [ "$option_develop" = true ]; then
		# returns 0 or 1 depending on whether remote dev branch exists:
		has_dev=$(git ls-remote --heads $url refs/heads/$dev_branch_name | wc -l)
		if [ "$has_dev" = 1 ] ; then
			echo "Checking out dev version of $proj"
			cd $path
			git fetch
			git switch dev
		fi
	fi
done
