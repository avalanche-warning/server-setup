#!/usr/bin/env bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the AWSOME installer.

# This script creates developer accounts to work on the code base.
# They are quite powerful and may:
#   - Modify all code
#   - Access directories owned by the operational users:
#     - the home directories of all operational users
#     - the public web space of all operational users
#   - Access directories owned by high level groups:
#     - common software build locations
#   - Modify git repositories that were cloned with a different user
#   - Manipulate the toolchains' venvs

set -e
set -x

if [[ -z ${AWSOME_BASE} ]]; then
	export AWSOME_BASE=/opt/awsome/code
	echo "create_devops.sh set AWSOME_BASE to $AWSOME_BASE"
fi
if [[ -z ${AWSOME_HOME} ]]; then
	export AWSOME_HOME=/home
	echo "create_devops.sh set AWSOME_HOME to $AWSOME_HOME"
fi
if [[ -z ${AWSOME_VENVS} ]]; then
	export AWSOME_VENVS=/opt/awsome/venvs
	echo "create_devops.sh set AWSOME_VENVS to $AWSOME_VENVS"
fi

cwdu=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source ${cwdu}/common.sh

accounts=$1

if [[ -z ${accounts} ]]; then # no devops
	return
fi

# if there are devops, install some system monitoring software:
apt-get install -y glances

getent group devops &>/dev/null || groupadd devops # create "devops" group if n/a
chown -R root:devops $AWSOME_BASE
chmod -R 775 $AWSOME_BASE
chown -R root:devops $AWSOME_VENVS
chmod -R 775 $AWSOME_VENVS
# (note that you need to re-run this script when re-downloading AWSOME)

IFS="," read -ra dev_accounts <<< $accounts # comma-separated list of account names
for dev in "${dev_accounts[@]}"; do
	if ! user_exists "$dev"; then
		# gecos: full name,room number,work phone,home phone,other
		adduser --home ${AWSOME_HOME}/${dev} --disabled-password --gecos "" $dev
		adduser $dev devops
		usermod -a -G "opera,smp,snowpack,wrf" $dev
		usermod -a -G "data,snow-cover,universal,visualization,weather" $dev
	fi
	# root is the file owner for code, so disable git's "dubious ownership"
	# (can not be done recursively, only for "*", also does not support groups):
	su - $dev -c "git config --global --add safe.directory '*'"
done
