#!/usr/bin/env python3
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
"""
This is the AWSOME installer.
It features a terminal GUI but can also be scripted with flags.
"""

import argparse
from collections import OrderedDict
import curses
import curses.textpad
from datetime import datetime
from enum import IntEnum
import logging
import os
import selectors
import subprocess
import sys

class ODict(OrderedDict):
    """An ordered dictionary class with easy index access.

    The user options are tied to a sorted list of GUI items for them. In order
    to be able to reference them by index and key we use a sorted dictionary.
    """
    def __init__(self: "ODict", lst: list):
        super().__init__(lst)
    def __getitem__(self: "ODict", pos: int | str):
        """Retrieves an ODict entry by index or label."""
        if isinstance(pos, str): # normal key string access
            return super().get(pos)
        if isinstance(pos, int): # indexed access
            return list(self.values())[pos]
        raise IndexError(f'Invalid type "{type(pos)}" for ODict key.')

class _CursesColor(IntEnum): # named integers for terminal colors
    WHITE = 0
    RED = 1
    GREEN = 2
    BLUE = 3
    MAGENTA = 4

class GUI():
    """A collection of screens constituting our user interface."""
    def __init__(self, screens: dict[str, curses.window], textbox: curses.textpad.Textbox):
        self._screens = {}
        self._screens["banner"] = screens["banner"]
        self._screens["options"] = screens["options"]
        self._screens["status"] = screens["status"]
        self._screens["textbox"] = screens["textbox"]
        self._screens["output"] = screens["output"]
        self._screens["progress"] = screens["progress"]
        self._input_textfield = textbox
        self._sizeok: bool = True
        self.enter_pressed_once: bool = False # ENTER must be confirmed

    def __getitem__(self, screen: str) -> curses.window:
        """Returns the curses window object tagged with name 'screen'."""
        return self._screens[screen]

    def __setitem__(self, screen: str, window: curses.window):
        """Sets the curses window object tagged with name 'screen'."""
        self._screens[screen] = window

    @property
    def input_textfield(self):
        """Returns the interactive user input textfield."""
        return self._input_textfield
    @input_textfield.setter
    def input_textfield(self, input_textfield: curses.textpad.Textbox):
        """Sets the interactive textfield object."""
        self._input_textfield = input_textfield
    @property
    def sizeok(self):
        """Returns whether the screen is big enough or not."""
        return self._sizeok
    @sizeok.setter
    def sizeok(self, sizeok: bool):
        """Sets whether the screen is big enough or not."""
        self._sizeok = sizeok
    @property
    def enter_pressed_once(self):
        """Returns whether the user has already pressed ENTER once."""
        return self._enter_pressed_once
    @enter_pressed_once.setter
    def enter_pressed_once(self, enter_pressed_once: curses.textpad.Textbox):
        """Sets whether the user has already pressed ENTER once."""
        self._enter_pressed_once = enter_pressed_once

    def progress(self, task_nr: int, nr_of_tasks: int):
        """Show a progress bar at the bottom of the screen.

        Arguments:
            gui: The collection of screens constituting our user interface.
            task_nr: Number of currently running task.
            nr_of_tasks: Total number of tasks.
        """
        if self._screens["progress"] is None: # GUI is disabled
            return
        _, screenw = self._screens["progress"].getmaxyx()
        infostr = f"Task {task_nr}/{nr_of_tasks} "
        screenw = screenw - len(infostr) - 2 #- 2 for brackets
        task_nr = min(task_nr, nr_of_tasks)
        if nr_of_tasks == 0:
            pp = 0.
        else:
            pp = task_nr / nr_of_tasks
        occupied = round(pp * screenw)
        printscr(self._screens["progress"],
            text=infostr + "[" + "#" * occupied + "." * (screenw - occupied) + "]")

    def print_info(self):
        """Print an info string to the place the input textfield sits at."""
        lbl = "Press SPACE to edit or toggle, ENTER to confirm, q to quit."
        printscr(self._screens["textbox"], text=lbl)

log: logging.Logger = logging.Logger(__name__) # init to file later
_project_url: str = "https://gitlab.com/avalanche-warning/"
_project_online_help: str = "https://gitlab.com/groups/avalanche-warning/-/wikis/home"

def _make_banner() -> str:
    """AWSOME welcome text and version info as window header."""
    banner = r"""
  Welcome to the
    Avalanche Warning Service Operational Meteo Environment
     ___ _      __________  __  _______
    / _ | | /| / / __/ __ \/  |/  / __/
   / __ | |/ |/ /\ \/ /_/ / /|_/ / _/
  /_/ |_|__/|__/___/\____/_/  /_/___/


  Version {}
  {}
""".lstrip("\n").rstrip("\n")
    version, date = _get_version()
    banner = banner.format(f"{version}; {date}", _project_url)
    return banner

def _spath() -> str:
    """Returns the path of caller's script file."""
    return os.path.dirname(os.path.realpath(__file__)) + "/"

_default_texts = { # default values for textbox input fields
    "opt-host": "avalanche.report",
    "opt-home": "/home",
    "opt-base": "/opt/awsome/code",
    "opt-venvs": "/opt/awsome/venvs",
    "opt-storage": "/mnt/storage-box",
    "opt-secrets": "/etc/awsome.ini",
    "opt-devops": "",
} # length must match number of textboxes

# our GUI types are:
#  - checkbox: on/off toggle for optional setup scripts to run
#  - setup-checkbox: on/off toggle for settings for the installer itself
#  - separator: Empty space
#  - textbox: Display of text, all textboxes use the same interactive input field
# pylint: disable=line-too-long
_options: ODict = ODict([ # user-settable options
    ("mod-snowcover", {"label": "Snow-cover modules", "type": "checkbox", "checked": False}),
    ("mod-weather", {"label": "Weather modules", "type": "checkbox", "checked": False}),
    ("mod-wrf", {"label": "WRF NWP model", "type": "checkbox", "checked": False}),
    ("mod-smp", {"label": "SnowMicroPenetrometer toolchain", "type": "checkbox", "checked": False}),
    ("mod-wms", {"label": "WMS Server", "type": "checkbox", "checked": False}),
    ("mod-visualization", {"label": "Visualization tools", "type": "checkbox", "checked": False}),
    ("mod-winterreport", {"label": "Winter report web apps", "type": "checkbox", "checked": False}),
    ("mod-openavy", {"label": "OpenAvy R suite", "type": "checkbox", "checked": False}),
    ("sep1", {"type": "separator", "label": ""}),
    ("setup-update", {"label": "Update AWSOME modules only", "type": "setup-checkbox", "checked": False}),
    ("setup-develop", {"label": "Install newest unreleased version", "type": "setup-checkbox", "checked": False}),
    ("setup-small", {"label": "Produce a smaller setup", "type": "setup-checkbox", "checked": False}),
    ("setup-reinstall", {"label": "Reinstall software", "type": "setup-checkbox", "checked": False}),
    ("sep2", {"type": "separator", "label": ""}),
    ("opt-host", {"label": "Host name", "type": "textbox", "text": _default_texts["opt-host"]}),
    ("opt-home", {"label": "Home location", "type": "textbox", "text": _default_texts["opt-home"]}),
    ("opt-base", {"label": "Code base location", "type": "textbox", "text": _default_texts["opt-base"]}),
    ("opt-venvs", {"label": "Virtual environments", "type": "textbox", "text": _default_texts["opt-venvs"]}),
    ("opt-storage", {"label": "Data storage location", "type": "textbox", "text": _default_texts["opt-storage"]}),
    ("opt-secrets", {"label": "Location of secrets file", "type": "textbox", "text": _default_texts["opt-secrets"]}),
    ("opt-devops", {"label": "Add developer accounts", "type": "textbox", "text": _default_texts["opt-devops"]}),
])

_info_options: list = [ # infotexts for user settings
    "Install toolchains for snowpack stratigraphy now- and forecasts.",
    "Install weather modules (solar, satellites, ...)",
    "Install the WRF numerical weather prediction model.",
    "Install the SnowMicroPenetrometer forecasts toolchain.",
    "Install a WebMapService for serving weather station data.",
    "Install visualization tools including a dashboard",
    "Install interactive web apps for report generation.",
    "Install standalone R package powering SNOWPACK simulations.",
    "",
    "Only update the code base and re-configure the server for it.",
    "Switch to the developer's version.",
    "Some modules may produce/download less data with this option.",
    "Force re-installation of all software modules.",
    "",
    "Set the host name the machine is reachable at.",
    "Set the users' home directories' locations.",
    "Set the directory all of AWSOME's code will be put in.",
    "Select location the modules' virtual environments will be created at.",
    "Path where toolchains can store big amounts of data.",
    "Path to an ini file containing credentials for some model chains.",
    "Create users on the server who can modify all code (comma-separated).",
] # length must match number of entries in _options

_mandatory_calls: list = [ # system calls that will always be executed
    ["Updating system software...", [_spath() + "../scripts/update_system.sh"]],
    ["Installing base software...", [_spath() + "../scripts/install_base_software.sh"]],
    ["Creating users...", [_spath() + "../scripts/create_users.sh"]],
    ["Downloading AWSOME...", [_spath() + "../scripts/download_awsome.sh"]],
    ["Installing global software...", [_spath() + "../scripts/install_global_software.sh"]],
    ["Installing webserver...", [_spath() + "../scripts/install_webserver.sh"]],
    ["Installing database...", [_spath() + "../scripts/install_database.sh"]],
]
_optional_calls: list = [ # system calls dependent on checked user options
    ["Installing snow-cover modules...", [_spath() + "../scripts/install_module_snowcover.sh"]],
    ["Installing weather modules...", [_spath() + "../scripts/install_module_weather.sh"]],
    ["Installing WRF...", [_spath() + "../scripts/install_module_wrf.sh"]],
    ["Installing SMP toolchain...", [_spath() + "../scripts/install_module_smp.sh"]],
    ["Installing WMS server...", [_spath() + "../scripts/install_module_wms.sh"]],
    ["Installing visualization tools...", [_spath() + "../scripts/install_module_wms.sh"]],
    ["Installing winter report web apps...", [_spath() + "../scripts/install_module_winterreport.sh"]],
    ["Installing OpenAvy...", [_spath() + "../scripts/install_module_openavy.sh"]],
] # length must match number of "checkbox" items in _options
_update_scripts: list = [ # scripts to run instead of all others when "update" is selected:
    ["Re-downloading AWSOME...", [_spath() + "../scripts/download_awsome.sh"]],
    ["Resetting devops permissions...", [_spath() + "../scripts/create_devops.sh"]],
]
# pylint: enable=line-too-long

def _set_env_vars():
    """Make some user settings available to the installation scripts."""
    _fenv = "/etc/environment" # global environment variables

    aws_envs = {
        "AWSOME_HOME": _options["opt-home"]["text"],
        "AWSOME_BASE": _options["opt-base"]["text"],
        "AWSOME_VENVS": _options["opt-venvs"]["text"],
        "AWSOME_STORAGE": _options["opt-storage"]["text"],
        "AWSOME_SECRETS": _options["opt-secrets"]["text"],
        "AWSOME_HOST": _options["opt-host"]["text"],
    }
    if _options["setup-small"]["checked"]:
        aws_envs["AWSOME_SMALL"] = "1"

    with open(_fenv, "r", encoding="utf-8") as envfile:
        lines = envfile.readlines()
    with open(_fenv, "w", encoding="utf-8") as envfile:
        for line in lines:
            if line.strip("\n").startswith("AWSOME_"):
                continue
            envfile.write(line)
        for key, value in aws_envs.items():
            envfile.write(f"{key}={value}\n") # for all new login shells
            os.environ[key] = aws_envs[key] # for the setup process

def _get_version() -> tuple[str, str]:
    """Use git describe to fetch this repository's tag or hash and commit date thereof."""
    # current tag or if unreleaed the commit hash:
    vv = subprocess.check_output(["git", "-C", _spath(), "describe", "--always", "--tags"])
    vvs = vv.decode("ascii").strip()
    # date of last tag or hash:
    dt = subprocess.check_output(
        ["git", "-C", _spath(), "show", "--no-patch", "--no-notes", "--pretty='%cI'", vvs])
    dts = dt.decode("ascii").strip().strip("'")
    dts = dts.rsplit("\n", maxsplit=1)[-1] # remove tag info if available
    dts = dts.lstrip("'") # remove trailing '
    dts = dts.split("+", maxsplit=1)[0] # remove time zone info
    dtt = datetime.strptime(dts, "%Y-%m-%dT%H:%M:%S")
    dts = dtt.strftime("%A, %Y-%m-%d")
    return vvs, dts

def _init_curses():
    """Helper function to set curses' behavior."""
    curses.curs_set(0)  # invisible cursor
    curses.init_pair(_CursesColor.RED, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(_CursesColor.GREEN, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(_CursesColor.BLUE, curses.COLOR_BLUE, curses.COLOR_BLACK)
    curses.init_pair(_CursesColor.MAGENTA, curses.COLOR_MAGENTA, curses.COLOR_BLACK)

def _init_logger(logger: logging.Logger):
    """Initialize the logger.

    Note: follow the log via e. g. "less +F installation.log"

    Arguments:
        logger: The logging opject.
    """
    logger = logging.getLogger(__name__)
    log_form = "%(asctime)s,%(levelname)s,%(name)s,%(module)s,%(funcName)s,%(lineno)d: %(message)s"
    logfilename = os.path.join(_spath(), "..", "installation.log") # fixed logfile path
    try:
        logging.basicConfig(filename=logfilename, filemode="w", format=log_form,
            encoding="utf-8", level=logging.DEBUG)
    except PermissionError: # installer was correctly ran as root before, but not now (probably)
        logging.basicConfig(filename="/dev/null")
    return logger

# pylint: disable=too-many-arguments, too-many-positional-arguments
def printscr(screen: curses.window | None, text: str="", line: int=0,
        column: int | None=None, clear_to_bottom: bool=False,
        color: int | None=_CursesColor.WHITE,
        box: bool=False, refresh: bool=True):
    """Print line of text to some screen.

    Arguments:
        screen: The curses window to print to or 'None' to print to console.
        line: Line number to print to.
        clear_to_bottom: Clear the whole rest of the screen?
        color: Index of color to use (see class _CursesColor).
        box: Re-draw a box around the screen?
        refresh: Set to false to only overwrite parts (no flickering).
    """
    if text is None:
        return
    if screen is None: # GUI is disabled
        print(text) # --> simple terminal output
        return

    if column is None:
        column = 2 if box else 0 # the starting column needs to respect a box
    _color = None
    try:
        if color is not None:
            screen.addstr(line, column, text, curses.color_pair(color))
        else: # Passing None as color allows for curses styling like A_REVERSE
            screen.addstr(line, column, text)
    except curses.error:
        pass # just truncate out-of-bounds text
    if clear_to_bottom:
        screen.clrtobot()
    else:
        screen.clrtoeol()
    if box:
        screen.box()
    if refresh:
        screen.refresh()
# pylint: enable=too-many-arguments

def _get_argstring() -> list:
    """These are the arguments that are passed to the external installation scripts."""
    cmd_list = ["update", "develop", "reinstall"]
    cmdline: list = []
    for cmd in cmd_list:
        if _options[f"setup-{cmd}"]["checked"]:
            cmdline.append(f"--{cmd}")
    return cmdline

def _run_update_scripts(cmdline: list, gui: GUI | None) -> int:
    """Only run the update scripts."""
    total = len(_update_scripts)
    current = 0 # for progress bar
    proc_exit = 0
    for call in _update_scripts:
        current += 1
        if gui:
            gui.progress(current, total)
        cmd = list(call[1]) + cmdline
        proc_exit += run_process(gui, status_text=str(call[0]), cmd=cmd)
    errtext = ""
    if proc_exit != 0:
        errtext = " (with errors)"
    printscr(gui["output"] if gui else None,
        text=f"Update AWSOME finished{errtext}.", clear_to_bottom=True)
    return proc_exit == 0

def run_scripts(gui: GUI | None) -> int:
    """Main iteration over external installation scripts.

    Arguments:
        gui: The collection of screens constituting our user interface.
    Returns:
        0 if all scripts have exited with 0, 1 otherwise.
    """
    cmdline = _get_argstring() # this list of cmd args will be passed on
    if _options["setup-update"]["checked"]:
        return _run_update_scripts(cmdline, gui) == 0

    total = len(_mandatory_calls)
    current = 0 # for progress bar
    # count number of optional tasks to perform:
    for op in _options:
        if _options[op]["type"] == "checkbox" and _options[op]["checked"]:
            total += 1
    nr_not_successful = 0 # may merely be a warning - do not abort
    printscr(gui["output"] if gui else None) # clear

    proc_exit = 0 # accumulative return codes
    # scripts independent of user option checkboxes:
    for call in _mandatory_calls:
        current += 1
        if gui:
            gui.progress(current, total)
        cmd = list(call[1]) + cmdline
        proc_exit = run_process(gui, status_text=str(call[0]), cmd=cmd)
        nr_not_successful += proc_exit # depending on the ret code might not be actual #

    # scripts that run depending on an on/off toggle:
    for ii, cb in enumerate(_options):
        if _options[cb]["type"] == "checkbox":
            if _options[cb]["checked"]:
                current += 1
                if gui:
                    gui.progress(current, total)
                cmd = list(_optional_calls[ii][1]) + cmdline
                proc_exit = run_process(gui, str(_optional_calls[ii][0]), cmd=cmd)
                nr_not_successful += proc_exit

    # specific scripts:
    dev_accounts = _options["opt-devops"]["text"].strip()
    if dev_accounts:
        cmd = [_spath() + "../scripts/create_devops.sh", dev_accounts]
        proc_exit += run_process(gui, str("Creating dev users..."), cmd=cmd)
        log.info(proc_exit)
        nr_not_successful += proc_exit
    all_successful = proc_exit == 0

    stext = "All done."
    printscr(gui["status"] if gui else None, stext, color=_CursesColor.GREEN)
    log.info(stext)
    if all_successful and gui:
        gui["output"].clear()
        gui["output"].refresh()
    else:
        etext = "Errors or warnings have occurred"
        printscr(gui["output"] if gui else None, color=_CursesColor.RED,
            text=etext + "- check the logfile.")
        log.error("%s.", etext)

    return 0 if all_successful else 1

def run_process(gui, status_text: str, cmd: list[str]) -> int:
    """Run a system command.

    Arguments:
        gui: The collection of screens constituting our user interface.
        cmd: System command to run.
    Returns:
        Return code of the process.
    """
    # info about what is being ran:
    printscr(gui["status"] if gui else None, status_text,
        color=_CursesColor.GREEN, clear_to_bottom=True)
    printscr(gui["output"] if gui else None) # clear

    # run the system command in a pipe and process output as it is coming in:
    log.info('Executing "%s"', cmd)
    with subprocess.Popen(cmd, stdin=subprocess.PIPE, stderr=subprocess.PIPE,
            stdout=subprocess.PIPE) as process:
        sel = selectors.DefaultSelector() # for simultaneous read-in of out and err
        sel.register(process.stdout, selectors.EVENT_READ) # type: ignore
        sel.register(process.stderr, selectors.EVENT_READ) # type: ignore

        buffer = ""
        errbuffer = ""
        while True:
            # We do it as manually as we can char-by-char with a buffer acting on
            # newlines. This gives the possibility to read from and write to all pipes live:
            for key, _ in sel.select(): # query the stdout and stderr pipes simultaneously
                char = key.fileobj.read(1) # type: ignore
                if char:
                    char = char.decode("utf-8", "ignore") # throw away unlucky splits of multi-bytes
                    if key.fileobj is process.stdout:
                        buffer = stdout_char_received(char, buffer, gui)
                    else:
                        errbuffer = stderr_char_received(char, errbuffer, gui)

            return_code = process.poll()
            if return_code is not None: # process finished
                if return_code != 0:
                    log.error("Process exited with return code %i", return_code)
                break

        process.stdout.close() # type: ignore
        process.stderr.close() # type: ignore
        process.stdin.close() # type: ignore
    return return_code # if != 0 this may merely be unimportant warnings

def stderr_char_received(char: str, buffer: str, gui: GUI) -> str:
    """Data received from the stderr pipe.

    Arguments:
        char: Received data.
        buffer: Buffer of stderr pipe -> we put together its single lines here.
        gui: The collection of screens constituting our user interface.
    Returns:
        The modified buffer.
    """
    if char == "\n":
        log.warning(buffer)
        printscr(gui["output"] if gui else None,
            text=buffer, color=_CursesColor.RED, clear_to_bottom=True)
        buffer = ""
    else:
        buffer += char
    return buffer

def stdout_char_received(char: str, buffer: str, gui: GUI) -> str:
    """Data received from the stdout pipe.

    Arguments:
        char: Received data.
        buffer: Buffer of stdout pipe -> we put together and parse its single
            lines here.
        gui: The collection of screens constituting our user interface.
    Returns:
        The modified buffer.
    """
    if char == "\n":
        log.debug(buffer)
        printscr(gui["output"] if gui else None, text=buffer, clear_to_bottom=True)
        buffer = ""
    else:
        buffer += char
        # (there could be actions here depending on some program output / prompt)
    return buffer

def key_up_pressed(emph: int) -> int:
    """User has navigated up in the options.

    Arguments:
        emph: Index of currently selected option.
    Returns:
        Index of newly selected option.
    """
    skip = 1
    try: # if the next item exists and its a separator skip one more line:
        if _options[emph - 1]["type"] == "separator":
            skip = 2 # does not support consecutive separators
    except IndexError:
        pass
    emph = (emph - skip) % len(_options) # wrap around settings
    return emph

def key_down_pressed(emph: int) -> int:
    """User has navigated down in the options.

    Arguments:
        emph: Index of currently selected option.
    Returns:
        Index of newly selected option.
    """
    skip = 1
    try:
        if _options[emph + 1]["type"] == "separator":
            skip = 2
    except IndexError:
        pass
    emph = (emph + skip) % len(_options)
    return emph

def key_space_pressed(emph: int, gui: GUI):
    """User has pressed the spacebar.

    Arguments:
        emph: Index of currently selected option.
        gui: The collection of screens constituting our user interface.
    """
    if not gui.sizeok:
        return

    if _options[emph]["type"] == "separator":
        return
    if _options[emph]["type"] == "checkbox" or _options[emph]["type"] == "setup-checkbox":
        _options[emph]["checked"] = not _options[emph]["checked"]
    elif _options[emph]["type"] == "textbox":
        curses.curs_set(1)
        gui["textbox"].clear()
        gui["textbox"].addstr(_options[emph]["text"])
        user_entry = gui.input_textfield.edit().strip()
        curses.curs_set(0)
        gui.print_info()
        _options[emph]["text"] = user_entry
    _validate_options(gui) # user input sanity check

def key_enter_pressed(gui: GUI) -> int:
    """User has pressed ENTER (twice).

    Arguments:
        gui: The collection of screens constituting our user interface.
    Returns:
        0 if all processes exited successfuly, 1 otherwise.
    """
    # If the user hits ENTER they are prompted to do so again in order to
    # avoid accidental starts when trying to set an option:
    if gui.enter_pressed_once: # now is the 2nd time
        gui.enter_pressed_once = False # for next run
        exit_code = start_installation(gui)
    else:
        gui.enter_pressed_once = True
        printscr(gui["textbox"], color=_CursesColor.GREEN,
            text="Press ENTER again to start the installation, any other key to abort.")
        itext = "The installer will install base system software and the modules you have selected."
        printscr(gui["output"], text=itext)
        return 0
    return exit_code

def start_installation(gui: GUI | None) -> int:
    """Starts the installation process in both terminal and GUI mode.

    Arguments:
        gui: The collection of screens constituting our user interface.
    Returns:
        0 if all processes exited successfuly, 1 otherwise.
    """
    _set_env_vars() # user settings to environment variables
    exit_code = run_scripts(gui)
    return exit_code

def _validate_options(gui: GUI):
    """After each user interaction perform a sanity check for the inputs.

    Arguments:
        gui: The collection of screens constituting our user interface.
    """
    # some settings like the home directories must not be empty:
    mandatory_texts = ["opt-host", "opt-home", "opt-base", "opt-venvs", "opt-storage"]
    for txt in mandatory_texts:
        if not _options[txt]["text"]:
            printscr(gui["textbox"],
                f"{_options[txt]['label']} can not be empty.",
                color=_CursesColor.RED) # warning until next key stroke
            _options[txt]["text"] = _default_texts[txt]

    # some modules depend on each other:
    dependent_pairs = [
        ("mod-weather", "mod-snowcover"), # first depends on second
        ("mod-winterreport", "mod-visualization"),
    ]
    for mod in dependent_pairs:
        if _options[mod[0]]["checked"]:
            if not _options[mod[1]]["checked"]: # we need to interfere
                printscr(gui["textbox"],
                    f"{_options[mod[1]]['label']} is mandatory for {_options[mod[0]]['label']}.",
                    color=_CursesColor.RED)
                _options[mod[1]]["checked"] = True

    # exclusive options deselect all others:
    exclusive_options = { # lists of options that are allowed if the key option is set
        "setup-update": ["setup-develop"],
    }
    for excl, allowed in exclusive_options.items():
        if _options[excl]["checked"]:
            for opt in _options:
                if opt == excl: # don't deselect right away
                    continue
                if _options[opt]["type"] == "checkbox" or _options[opt]["type"] == "setup-checkbox":
                    # only warn if necessary:
                    if _options[opt]["checked"] and opt not in allowed:
                        prnt_allowed = ",".join(allowed)
                        _options[opt]["checked"] = False
                        printscr(gui["textbox"], color=_CursesColor.RED,
                            text=f"{_options[excl]['label']} only allows {prnt_allowed}")
            break # only need to look at first checked exclusive option

def _build_argparser() -> argparse.Namespace:
    """Command line args to use in the installer and/or pass to the scripts.

    The way it works is the following:
      - When entering the GUI, a couple of default options are selected.
      - Additional GUI options can be pre-selected by passing the mods as cmd args.
      - When bypassing the GUI, all modules are disabled by default.
      - Individual modules can then be enabled with cmd line args.
      - The same can be done with text inputs flags.
      - If nothing is enabled the installer will still run mandatory scripts.

    Returns:
        The parsed command line args.
    """
    parser = argparse.ArgumentParser(
        prog="AWSOME installer",
        description="This program sets up and configures a fully operational AWSOME machine.",
        epilog=f"Online help: {_project_online_help}"
    )
    # options to toggle modules:
    parser.add_argument("--mod_snowcover",
        help="install snow-cover module", action="store_true")
    parser.add_argument("--mod_weather",
        help="install weather module", action="store_true")
    parser.add_argument("--mod_wrf",
        help="install WRF module", action="store_true")
    parser.add_argument("--mod_smp",
        help="install SMP module", action="store_true")
    parser.add_argument("--mod_wms",
        help="install WMS module", action="store_true")
    parser.add_argument("--mod_visualization",
        help="install visualization module", action="store_true")
    parser.add_argument("--mod_winterreport",
        help="install winter-report module", action="store_true")
    parser.add_argument("--mod_openavy",
        help="install OpenAvy module", action="store_true")
    parser.add_argument("--mod_full",
        help="install all modules", action="store_true")

    # install routine options:
    parser.add_argument("--nogui",
        help="bypass graphical user interface", action="store_true")
    parser.add_argument("--update",
        help="only update AWSOME modules", action="store_true")
    parser.add_argument("--develop",
        help="install newest unreleased version", action="store_true")
    parser.add_argument("--small",
        help="install smaller version of AWSOME", action="store_true")
    parser.add_argument("--reinstall",
        help="reinstall all software", action="store_true")
    parser.add_argument("--remove",
        help="remove setup files", action="store_true")

    # text options:
    parser.add_argument("--host", help="host address of webserver")
    parser.add_argument("--home", help="location of users' home directories")
    parser.add_argument("--base", help="location of AWSOME's code base")
    parser.add_argument("--venvs", help="location of virtual environments")
    parser.add_argument("--storage", help="path to large storage space")
    parser.add_argument("--secrets", help="path to secrets file")
    parser.add_argument("--devops", help="comma-separated list of developers")

    cmd_args = parser.parse_args()
    return cmd_args

def _init_options_from_cmd(cmd_args: argparse.Namespace):
    """Set default option values if given as cmd line args.

    If the flag is not set and we are in GUI mode we use the
    defaults set in the main entry point.
    """
    _options["mod-snowcover"]["checked"] = cmd_args.mod_snowcover
    _options["mod-weather"]["checked"] = cmd_args.mod_weather
    _options["mod-wrf"]["checked"] = cmd_args.mod_wrf
    _options["mod-smp"]["checked"] = cmd_args.mod_smp
    _options["mod-wms"]["checked"] = cmd_args.mod_wms
    _options["mod-visualization"]["checked"] = cmd_args.mod_visualization
    _options["mod-winterreport"]["checked"] = cmd_args.mod_winterreport
    _options["mod-openavy"]["checked"] = cmd_args.mod_openavy
    if cmd_args.mod_full:
        for op in _options:
            if op.startswith("mod-"):
                _options[op]["checked"] = True

    _options["setup-update"]["checked"] = cmd_args.update
    _options["setup-develop"]["checked"] = cmd_args.develop
    _options["setup-small"]["checked"] = cmd_args.small
    _options["setup-reinstall"]["checked"] = cmd_args.reinstall

    if cmd_args.host:
        _options["opt-host"]["text"] = cmd_args.host
    if cmd_args.home:
        _options["opt-home"]["text"] = cmd_args.home
    if cmd_args.base:
        _options["opt-base"]["text"] = cmd_args.base
    if cmd_args.venvs:
        _options["opt-venvs"]["text"] = cmd_args.venvs
    if cmd_args.storage:
        _options["opt-storage"]["text"] = cmd_args.storage
    if cmd_args.secrets:
        _options["opt-secrets"]["text"] = cmd_args.secrets
    if cmd_args.devops:
        _options["opt-devops"]["text"] = cmd_args.devops

def _get_option_label(option: dict) -> str:
    """Get display text of some user settings item.

    This depends for the GUI element type, for example a checkbox
    is toggled.

    Arguments:
        option: The currently selected option (from _options).
    Returns:
        The ui text to display for the option.
    """
    if option["type"] == "separator":
        label = option["label"]
    elif option["type"] == "checkbox" or option["type"] == "setup-checkbox":
        checked = "x" if option["checked"] else " "
        label = f'[{checked}] {option["label"]}'
    elif option["type"] == "textbox":
        label = f'{option["label"]}: {option["text"]}'
    else:
        label = "- invalid gui element -"
    return label

def _print_options(gui: GUI, emph: int):
    """Print the interactive user options to the screen.

    Arguments:
        gui: The collection of screens constituting our user interface.
        emph: Currently selected option index.
    """
    for ii, (_, opt) in enumerate(_options.items()):
        label = _get_option_label(opt)
        # select highlight style for current option
        if emph == ii:
            gui["options"].attron(curses.A_REVERSE) # reverse fg- and bg-color / highlight
        printscr(gui["options"], text=label, line=ii + 1, column=2,
            color=None, refresh=False) # update toggle checkmark only
        gui["options"].attroff(curses.A_REVERSE)

    # display info text of currently selected option index:
    printscr(gui["options"], _info_options[emph], len(_options) + 2,
        color=_CursesColor.MAGENTA, box=True)
    # note: a line wrap will produce some unwanted behavior

def _draw_options_screen(stdscr: curses.window, gui: GUI, emph: int):
    """Print the user options, or, if the screen is too small, a warning.

    Arguments:
        stdscr: The main screen hosting our windows.
        gui: The collection of screens constituting our user interface.
        emph: Currently selected option index.
    """
    if gui.sizeok:
        _print_options(gui, emph) # draw options ui
    else:
        msg = "Screen too small"
        # we still need at least 1 window to capture the resize message:
        try:
            gui["options"] = curses.newwin(1, len(msg) + 1, 0, 0)
        except curses.error:
            sys.exit("[E] Could not initialize screen")
        printscr(stdscr)
        printscr(gui["options"], msg, color=_CursesColor.RED, clear_to_bottom=True)

def _init_screen(stdscr: curses.window) -> GUI:
    """Create the graphical user interface.

    Arguments:
        stdscr: Main screen object passed by curses or 'None' to disable GUI.
    Returns:
        Newly created collection of screens constituting our user interface.
    """
    screens = {}

    banner = _make_banner()
    screenh, screenw = stdscr.getmaxyx()
    # check if we can display the GUI on this screen size (#s are heights of snames):
    min_height_required = len(banner.split("\n")) + len(_options) + 5 + 1 + 1 + 1 + 1
    if screenh < min_height_required:
        snames = ["banner", "options", "textbox", "status", "output", "progress"]
        for scr in snames:
            screens[scr] = curses.newwin(0, 0, 0, 0)
        gui = GUI(screens, curses.textpad.Textbox(screens["textbox"]))
        gui.sizeok = False
        return gui

    # syntax: curses.newwin(nlines, ncols, begin_y, begin_x)
    # vertically split window for banner:
    screens["banner"] = curses.newwin(len(banner.split("\n")), screenw, 0, 0)
    printscr(screens["banner"], banner, color=_CursesColor.BLUE)

    # window for user settings:
    screens["options"] = curses.newwin(len(_options) + 5, screenw, # +5 for box, 2x status, 1x empty
        screens["banner"].getmaxyx()[0] + 1, 0)
    screens["options"].keypad(True) # keyboard on
    screens["options"].box()

    # sub-window in user settings for textfield input:
    screens["textbox"] = screens["options"].derwin(1, screenw - 3, len(_options) + 3, 2)
    #^ h,w,y0,x0; "screenw - 3" for start col 1 + 2xbox

    # status line / headline for process output:
    _status_height = 1 # reserve this many lines for the status
    _content_height = screens["banner"].getmaxyx()[0] + screens["options"].getmaxyx()[0]
    screens["status"] = curses.newwin(_status_height, screenw, _content_height + 1, 0)
    printscr(screens["status"], text=
        "This script will install the necessary software to run AWSOME.")

    # rest of screen (minus box lines) for script outputs:
    screens["output"] = curses.newwin(screenh - _content_height - 3, screenw,
        _content_height + _status_height + 1, 0)
    screens["output"].refresh()
    printscr(screens["output"], text="Call with ./install.sh --help for command line options.")
    if os.geteuid() != 0: # only a warning if not root - try and fail to do everything...
        printscr(screens["output"], line=1, color=_CursesColor.RED,
            text="You are not root!\nSetting up the server will fail.")

    # last line is a progress bar:
    screens["progress"] = curses.newwin(1, screenw, screenh - 1, 0)

    # txt field in window that was created for it:
    _input_textfield = curses.textpad.Textbox(screens["textbox"], insert_mode=True)

    # create the GUI object:
    gui = GUI(screens, _input_textfield)
    gui.progress(0, 0)
    gui.print_info()
    return gui

def main(stdscr: curses.window | None, cmd_args: argparse.Namespace) -> int:
    """Main event loop.

    Arguments:
        stdscr: Main screen object passed by curses or 'None' to disable GUI.
        cmd_args: Parsed command line arguments.
    Returns:
        Exit code of the AWSOME installer.
    """
    _init_options_from_cmd(cmd_args)
    if stdscr is None: # GUI disabled via command line argument
        exit_code = start_installation(None)
        return exit_code
    _init_curses()
    gui = _init_screen(stdscr)

    exit_code = 0
    _emph = 0 # index of user-selected option
    while True: # console event loop - wait for keyboard input
        _draw_options_screen(stdscr, gui, _emph)
        key = gui["options"].getch() # now wait for user input
        if gui.sizeok:
            gui.print_info() # after each key stroke we clear sanity check warnings
        if key != ord("\n"):
            gui.enter_pressed_once = False
        if key == curses.KEY_UP or key == ord("k"): # additional vim bindings
            _emph = key_up_pressed(_emph)
        elif key == curses.KEY_DOWN or key == ord("j"):
            _emph = key_down_pressed(_emph)
        elif key == ord(" "):
            key_space_pressed(_emph, gui)
        elif key == ord("\n") and gui.sizeok: # no ENTER on invisible options
            exit_code = key_enter_pressed(gui)
        elif key == curses.KEY_RESIZE: # the console is being resized
            gui = _init_screen(stdscr) # re-draw with new size
            # if the screen grows big enough after being too small we re-init:
            if gui.sizeok:
                gui["options"].clear() # clear the box
                gui.print_info() # next time is only after next keystroke
        elif key == ord("q"):
            break
    return exit_code

if __name__ == "__main__":
    # Building the argparser is the first thing we do so that the --help
    # option can quit out without touching much:
    cmd_line_args = _build_argparser()
    log = _init_logger(log)
    # There are two ways to start the installer, in a terminal GUI and
    # in a simple console view:
    if cmd_line_args.nogui:
        log.info("GUI disabled via --nogui flag - entering terminal mode")
        EXIT_CODE = main(None, cmd_line_args)
    else:
        cmd_line_args.mod_snowcover = True # pre-select some defaults in the GUI
        cmd_line_args.mod_weather = True
        cmd_line_args.mod_visualization = True
        log.info("Launching GUI")
        EXIT_CODE = curses.wrapper(main, cmd_line_args) # start event loop
    log.log(logging.INFO if EXIT_CODE == 0 else logging.ERROR,
        "Exiting installer with exit code %i", EXIT_CODE)
    sys.exit(EXIT_CODE) # installer is done
