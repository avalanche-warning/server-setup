#!/usr/bin/env bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script installs the AWSOME framework on a server.

apt-get update
# Software needed by the installer itself:
apt-get install -y git > /dev/null
apt-get install -y python3 > /dev/null

setupd=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd ) # cwd
# run the installer transporting all cmd args but the 1st (filename):
/usr/bin/env python3 "${setupd}/gui/installer.py" "${@:1}"
exit_code=$? # return value of last command
exit $exit_code
