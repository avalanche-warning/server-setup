FROM ubuntu:24.04

#ARG AWSOME_BASE="/opt/awsome/code"
ARG AWSOME_BASE="."

COPY ./installer/ $AWSOME_BASE/installer/
COPY ./sites/ $AWSOME_BASE/sites/
COPY ./tests/ $AWSOME_BASE/tests/

RUN $AWSOME_BASE/installer/scripts/install_base_software.sh
