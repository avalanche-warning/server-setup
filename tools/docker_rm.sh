################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is not part of the installation process (but maybe the pipeline).
# It attempts to completely clear all Docker builds.

docker stop $(docker ps -qa) # stop all containers
docker rm $(docker ps -qa) # remove all containers
docker rmi -f $(docker images -qa) # remove all images
docker volume rm $(docker volume ls -qf) # remove all volumes
#docker network rm $(docker network ls -q) # remove all networks
