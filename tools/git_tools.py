################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This is a development script not in use by the installer.
# It automates git commands over AWSOME - careful!

import git
import os
import subprocess
import sys

_reporoot = "https://gitlab.com/avalanche-warning"

def _get_projects(coderoot):
    """Get a list of all of AWSOME's nested projects."""
    projects = [
        "data/fetchers/download",
        "data/fetchers/gfs",
        "data/fetchers/profile-obs",
        "data/fetchers/wiski",
        "data/static-data/dems/austria",
        "data/static-data/shapefiles",
        "data/provider",
        "snow-cover/model-chains/gridded-chain",
        "snow-cover/model-chains/smp-chain",
        "snow-cover/model-chains/snowobs-chain",
        "snow-cover/model-chains/stations-chain",
        "snow-cover/postprocessing/validation/nwp-model-analysis",
        "snow-cover/postprocessing/validation/profile-similarity-assessment",
        "snow-cover/postprocessing/qmah",
        "snow-cover/postprocessing/snowpacktools",
        "snow-cover/preprocessing/metadata",
        "snow-cover/preprocessing/nwp-models",
        "snow-cover/profsnow",
        "universal/packages/awsome-py",
        "universal/packages/awsome-r/awsome.ini",
        "universal/packages/awsome-r/awsome.pro",
        "universal/packages/awsome-r/awsome.smet",
        "universal/server/server-setup",
        "universal/server/toolchain",
        "universal/user-settings/domains",
        "visualization/dashboard",
        "visualization/upload",
        "visualization/web",
        "visualization/winter-report",
        "visualization/wms",
        "weather/satellites/sentinel",
        "weather/maps",
        "weather/solar",
        "weather/wrf",
    ]
    return projects

def print_active_branches(coderoot):
    """This function prints the active branch of each project."""
    projects = _get_projects(coderoot)
    for proj in projects:
        path = os.path.join(coderoot, proj)
        repo = git.Repo(path)
        branch = repo.active_branch
        print(f"{proj}: {branch.name}")

def commit_awsome(coderoot, commit_msg: str, author: str, email_author: str,
        committer: str=None, email_committer=None, push=False):
    """
    This function applies the same commit over all present changes within AWSOME.
    For example, if you batch replace some text in a file you could use this script
    to update all at once. Be careful.
    Arguments:
        coderoot: Parent directory of all of AWSOME's code.
        commit_msg: The commit message for all projects.
        author: Author of the commit.
        email_author: E-mail address of author.
        commiter: Commiter of the commit.
        email_committer: E-mail address of committer.
        push: If set to true, push to to the remote immediately.
    """
    if not committer:
        committer = author
    if not email_committer:
        email_committer = email_author

    projects = _get_projects(coderoot)
    for proj in projects:
        path = os.path.join(coderoot, proj)
        repo = git.Repo(path)
        repo.remotes.origin.fetch()

        diff = repo.index.diff(None)
        if len(diff) == 0:
            print(f"Skipping {proj} (nothing to commit)")
            continue
        repo.git.add(u=True)
        git_author = git.Actor(author, email_author)
        git_committer = git.Actor(committer, email_committer)
        repo.index.commit(commit_msg, author=git_author, committer=git_committer)
        if push:
            print(f"Pushing project {proj}")
            repo.remote().push()

def download_awsome(coderoot, update=False):
    """Clone list of AWSOME modules to a central place.

    Arguments:
        update: Pull all repositories instead of cloning them.
    """
    projects = _get_projects(coderoot)
    reporoot = "https://gitlab.com/avalanche-warning"
    for proj in projects:
        path = os.path.join(coderoot, proj)
        url = os.path.join(reporoot, proj)
        if not update and not os.path.exists(path):
            # mirror gitlab folder structure:
            os.makedirs(path, exist_ok=True)
            print(f"[i] Cloning into {url}")
            git.Repo.clone_from(url, path)
        elif update and os.path.exists(path):
            print(f"UPDATING {url}")
            repo = git.Repo(path)
            repo.remotes.origin.pull()

if __name__ == "__main__":

    # This script is intentionally kept in a way forcing hardcoding the action.

    commit_msg = None
    author = None
    email_author = None
    committer = None
    email_committer = None

    action = "print_active_branches"

    try:
        coderoot = os.environ["AWSOME_BASE"]
    except KeyError:
        sys.exit("Please provide the root code path in environment variable AWSOME_BASE.")

    if action == "commit_awsome":
        if author:
            commit_awsome(coderoot, commit_msg,  author, email_author,
                committer, email_committer, push=True)
        else:
            sys.exit("Hardcode your commit information.")
    elif action == "print_active_branches":
        print_active_branches(coderoot)
    elif action == "download_awsome":
        download_awsome(coderoot)
